fn solve_quadratic(a: f64, b: f64, c: f64) -> Vec<f64> {
    let discriminant = b * b - 4.0 * a * c;
    if discriminant > 0.0 {
        let root_discriminant = discriminant.sqrt();
        let x1 = (-b + root_discriminant) / (2.0 * a);
        let x2 = (-b - root_discriminant) / (2.0 * a);
        vec![x1, x2]
    } else if discriminant == 0.0 {
        let x = -b / (2.0 * a);
        vec![x]
    } else {
        vec![]
    }
}

fn main() {
    let a = 1.0;
    let b = 5.0;
    let c = 6.0;
    let roots = solve_quadratic(a, b, c);
    match roots.len() {
        0 => println!("No real roots"),
        1 => println!("One real root: x = {:.2}", roots[0]),
        _ => println!("Two real roots: x1 = {:.2}, x2 = {:.2}", roots[0], roots[1]),
    }
}
