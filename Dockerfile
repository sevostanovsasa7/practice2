FROM rust:latest

# Установка дополнительных пакетов (если нужно)
RUN apt-get update \
      && apt-get install -y sudo \
      && rm -rf /var/lib/apt/lists/*

# Копирование вашего исходного кода в образ
WORKDIR /usr/src/practice_2
COPY . .

# Сборка вашего приложения

RUN cargo build --release

# Помечаем собранный файл как исполняемый
CMD ["./target/release/practice_2"]
